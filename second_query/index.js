const express = require('express');
const app = express();

const express_graphql = require('express-graphql');
const { buildSchema } = require('graphql');

// importing data
const { courses } = require('./data.json');

// GrapqhQL Schema
const schema = buildSchema(`
  type Query {
    course(id: Int!): Course
    courses(topic: String): [Course]
  }

  type Mutation {
    updateCourseTopic(id: Int!, topic: String!): Course
  }

  type Course {
    id: Int
    title: String
    author: String
    description: String
    topic: String
    url: String
  }
`);

// GET ONE COURSE.
let getCourse = (args) => {
  let id = args.id;
  return courses.filter(course => {
      return course.id == id;
  })[0];
}

// GET ALL COURSES.
let getCourses = (args) => {
  if (args.topic) {
    let topic = args.topic;
    return courses.filter(course => course.topic === topic);
  } else {
    return courses;
  }
}

// ACTUALIZAR UN CURSO.
let updateCourseTopic = ({id, topic}) => {
  // ACTUALIZA.
  courses.map(course => {
    if (course.id === id) {
      course.topic = topic;
      return course;
    }
  });

  // RETORNA EL REGISTRO MODIFICADO.
  return courses.filter(course => course.id === id)[0];
}

// METODOS. SON PROPIEDADES A TRAVES DE FUNCIONES.
const root = {
  course: getCourse,
  courses: getCourses,
  updateCourseTopic: updateCourseTopic
};

// GraphQL endpoint. GRAFICA DE CONSULTAS.
app.use('/graphql', express_graphql({
  schema: schema,
  rootValue: root,
  graphiql: true
}));

app.listen(3000, () => console.log('server on port 3000'));
