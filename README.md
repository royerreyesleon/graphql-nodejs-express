GraphQL | Introducción Práctica a GraphQL con Nodejs y Express

https://www.youtube.com/watch?v=0Hvzg6PSosg


{
  message
}

QUERY VARIABLES
{
    "courseID": 1
}
query getSingleCourse($courseID: Int!){
  course(id: $courseID){
    title,
    author,
    description,
    topic,
    url
  }
}

QUERY VARIABLES
{
    "topic": "Javascript"
}
query getCourses($topic :String){
  courses(topic: $topic){
    id
    title
    author
    description
    topic
    url
  }
}

QUERY VARIABLES
{
    "courseID1": 1,
    "courseID2": 2 
}
query getCorusesWithFragments($courseID1: Int!, $courseID2: Int!){
  course1 :course(id: $courseID1){
    ...courseFields
  }
  course2 :course(id: $courseID2){
    ...courseFields
  }
}
fragment c on Course{
  title
  author
  description
  topic
  url
}

QUERY VARIABLES
{
	"id": 1,
	"topic": "nodeJS" 
}
mutation updateCourseTopic($id: Int!, $topic: String!){
   updateCourseTopic(id:$id, topic: $topic){
    ...courseFields
  }
}
fragment courseFields on Course{
  title
  author
  description
  topic
  url
}

# GraphQL with Nodejs and Express
![](screenshot.png)
![](screenshot2.png)
![](screenshot3.png)
![](screenshot4.png)
![](screenshot5.png)